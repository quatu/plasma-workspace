# Translation for kcm_users.po to Euskara/Basque (eu).
# Copyright (C) 2020-2023 This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
# SPDX-FileCopyrightText: 2023 KDE euskaratzeko proiektuko arduraduna <xalba@ni.eus>
#
# Translators:
# Iñigo Salvador Azurmendi <xalba@ni.eus>, 2020, 2021, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-02-24 00:38+0000\n"
"PO-Revision-Date: 2023-10-05 19:47+0200\n"
"Last-Translator: Iñigo Salvador Azurmendi <xalba@ni.eus>\n"
"Language-Team: Basque <kde-i18n-eu@kde.org>\n"
"Language: eu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 23.08.1\n"

#: src/fingerprintmodel.cpp:137 src/fingerprintmodel.cpp:244
#, kde-format
msgid "No fingerprint device found."
msgstr "Ez da hatz-marka gailurik aurkitu."

#: src/fingerprintmodel.cpp:317
#, kde-format
msgid "Retry scanning your finger."
msgstr "Saia zaitez zure hatza berriz eskaneatzen."

#: src/fingerprintmodel.cpp:319
#, kde-format
msgid "Swipe too short. Try again."
msgstr "Hatz pasaera laburregia. Berriz saiatu."

#: src/fingerprintmodel.cpp:321
#, kde-format
msgid "Finger not centered on the reader. Try again."
msgstr "Hatza ez dago irakurgailuan erdiratuta. Berriz saiatu."

#: src/fingerprintmodel.cpp:323
#, kde-format
msgid "Remove your finger from the reader, and try again."
msgstr "Kendu zure hatza irakurgailutik eta berriz saiatu."

#: src/fingerprintmodel.cpp:331
#, kde-format
msgid "Fingerprint enrollment has failed."
msgstr "Hatz-marka erroldatzea huts egin du."

#: src/fingerprintmodel.cpp:334
#, kde-format
msgid ""
"There is no space left for this device, delete other fingerprints to "
"continue."
msgstr ""
"Ez dago gailu honetarako lekurik, ezabatu beste hatz-marka batzuk "
"jarraitzeko."

#: src/fingerprintmodel.cpp:337
#, kde-format
msgid "The device was disconnected."
msgstr "Gailua deskonektatuta zegoen."

#: src/fingerprintmodel.cpp:342
#, kde-format
msgid "An unknown error has occurred."
msgstr "Errore ezezagun bat gertatu da."

#: src/ui/ChangePassword.qml:27 src/ui/UserDetailsPage.qml:170
#, kde-format
msgid "Change Password"
msgstr "Aldatu pasahitza"

#: src/ui/ChangePassword.qml:32
#, kde-format
msgid "Set Password"
msgstr "Ezarri pasahitza"

#: src/ui/ChangePassword.qml:55
#, kde-format
msgid "Password"
msgstr "Pasahitza"

#: src/ui/ChangePassword.qml:70
#, kde-format
msgid "Confirm password"
msgstr "Berretsi pasahitza:"

#: src/ui/ChangePassword.qml:89 src/ui/CreateUser.qml:68
#, kde-format
msgid "Passwords must match"
msgstr "Pasahitzak bat etorri behar du"

#: src/ui/ChangeWalletPassword.qml:16
#, kde-format
msgid "Change Wallet Password?"
msgstr "Zorroaren pasahitza aldatu?"

#: src/ui/ChangeWalletPassword.qml:25
#, kde-kuit-format
msgctxt "@info"
msgid ""
"Now that you have changed your login password, you may also want to change "
"the password on your default KWallet to match it."
msgstr ""
"Orain, saio-hasteko pasahitza aldatu duzunez, baliteke harekin bat etortzeko "
"zure KWallet lehenetsiko pasahitza aldatu nahi izatea."

#: src/ui/ChangeWalletPassword.qml:31
#, kde-format
msgid "What is KWallet?"
msgstr "Zer da KWallet?"

#: src/ui/ChangeWalletPassword.qml:41
#, kde-format
msgid ""
"KWallet is a password manager that stores your passwords for wireless "
"networks and other encrypted resources. It is locked with its own password "
"which differs from your login password. If the two passwords match, it can "
"be unlocked at login automatically so you don't have to enter the KWallet "
"password yourself."
msgstr ""
"KWallet pasahitz kudeatzaile bat da, haririk gabeko sareetarako eta "
"zifratutako beste baliabide batzuetarako pasahitzak gordetzen dituena. Bere "
"pasahitz propioarekin giltzatuta dago, saio-hasteko zure pasahitzarekiko "
"ezberdina. Bi pasahitzok bat datozenean, saio-hastean, giltzapetik "
"automatikoki askatu daiteke, KWallet-en pasahitza zuk zeuk sartu beharrik "
"izan ez dezazun."

#: src/ui/ChangeWalletPassword.qml:58
#, kde-format
msgid "Change Wallet Password"
msgstr "Aldatu zorroaren pasahitza"

#: src/ui/ChangeWalletPassword.qml:67
#, kde-format
msgid "Leave Unchanged"
msgstr "Utzi aldatzeke"

#: src/ui/CreateUser.qml:15
#, kde-format
msgid "Create User"
msgstr "Sortu erabiltzailea"

#: src/ui/CreateUser.qml:33 src/ui/UserDetailsPage.qml:132
#, kde-format
msgid "Name:"
msgstr "Izena:"

#: src/ui/CreateUser.qml:37 src/ui/UserDetailsPage.qml:140
#, kde-format
msgid "Username:"
msgstr "Erabiltzaile-izena:"

#: src/ui/CreateUser.qml:47 src/ui/UserDetailsPage.qml:150
#, kde-format
msgid "Standard"
msgstr "Arrunta"

#: src/ui/CreateUser.qml:48 src/ui/UserDetailsPage.qml:151
#, kde-format
msgid "Administrator"
msgstr "Administratzailea"

#: src/ui/CreateUser.qml:51 src/ui/UserDetailsPage.qml:154
#, kde-format
msgid "Account type:"
msgstr "Kontu mota:"

#: src/ui/CreateUser.qml:56
#, kde-format
msgid "Password:"
msgstr "Pasahitza:"

#: src/ui/CreateUser.qml:61
#, kde-format
msgid "Confirm password:"
msgstr "Berretsi pasahitza::"

#: src/ui/CreateUser.qml:78
#, kde-format
msgid "Create"
msgstr "Sortu"

#: src/ui/FingerprintDialog.qml:29
#, kde-format
msgid "Configure Fingerprints"
msgstr "Konfiguratu hatz-markak"

#: src/ui/FingerprintDialog.qml:38
#, kde-format
msgctxt "@action:button 'all' refers to fingerprints"
msgid "Clear All"
msgstr "Garbitu dena"

#: src/ui/FingerprintDialog.qml:45
#, kde-format
msgid "Add"
msgstr "Gehitu"

#: src/ui/FingerprintDialog.qml:54
#, kde-format
msgid "Cancel"
msgstr "Utzi"

#: src/ui/FingerprintDialog.qml:62
#, kde-format
msgid "Done"
msgstr "Eginda"

#: src/ui/FingerprintDialog.qml:84
#, kde-format
msgid "Enrolling Fingerprint"
msgstr "Hatz-marka erroldatzea"

#: src/ui/FingerprintDialog.qml:94
#, kde-format
msgid ""
"Please repeatedly press your right index finger on the fingerprint sensor."
msgstr ""
"Mesedez, sakatu zure eskuineko hatz erakuslea, behin eta berriz, hatz-marka "
"sentsorean."

#: src/ui/FingerprintDialog.qml:96
#, kde-format
msgid ""
"Please repeatedly press your right middle finger on the fingerprint sensor."
msgstr ""
"Mesedez, sakatu zure eskuineko hatz luzea, behin eta berriz, hatz-marka "
"sentsorean."

#: src/ui/FingerprintDialog.qml:98
#, kde-format
msgid ""
"Please repeatedly press your right ring finger on the fingerprint sensor."
msgstr ""
"Mesedez, sakatu zure eskuineko hatz nagia, behin eta berriz, hatz-marka "
"sentsorean."

#: src/ui/FingerprintDialog.qml:100
#, kde-format
msgid ""
"Please repeatedly press your right little finger on the fingerprint sensor."
msgstr ""
"Mesedez, sakatu zure eskuineko hatz txikia, behin eta berriz, hatz-marka "
"sentsorean."

#: src/ui/FingerprintDialog.qml:102
#, kde-format
msgid "Please repeatedly press your right thumb on the fingerprint sensor."
msgstr ""
"Mesedez, sakatu zure eskuineko erpurua, behin eta berriz, hatz-marka "
"sentsorean."

#: src/ui/FingerprintDialog.qml:104
#, kde-format
msgid ""
"Please repeatedly press your left index finger on the fingerprint sensor."
msgstr ""
"Mesedez, sakatu zure ezkerreko hatz erakuslea, behin eta berriz, hatz-marka "
"sentsorean."

#: src/ui/FingerprintDialog.qml:106
#, kde-format
msgid ""
"Please repeatedly press your left middle finger on the fingerprint sensor."
msgstr ""
"Mesedez, sakatu zure ezkerreko hatz luzea, behin eta berriz, hatz-marka "
"sentsorean."

#: src/ui/FingerprintDialog.qml:108
#, kde-format
msgid ""
"Please repeatedly press your left ring finger on the fingerprint sensor."
msgstr ""
"Mesedez, sakatu zure ezkerreko hatz nagia, behin eta berriz, hatz-marka "
"sentsorean."

#: src/ui/FingerprintDialog.qml:110
#, kde-format
msgid ""
"Please repeatedly press your left little finger on the fingerprint sensor."
msgstr ""
"Mesedez, sakatu zure ezkerreko hatz txikia, behin eta berriz, hatz-marka "
"sentsorean."

#: src/ui/FingerprintDialog.qml:112
#, kde-format
msgid "Please repeatedly press your left thumb on the fingerprint sensor."
msgstr ""
"Mesedez, sakatu zure ezkerreko erpurua, behin eta berriz, hatz-marka "
"sentsorean."

#: src/ui/FingerprintDialog.qml:116
#, kde-format
msgid ""
"Please repeatedly swipe your right index finger on the fingerprint sensor."
msgstr ""
"Mesedez, arin kolpatu zure eskuineko hatz erakuslea, behin eta berriz,  hatz-"
"marka sentsorean."

#: src/ui/FingerprintDialog.qml:118
#, kde-format
msgid ""
"Please repeatedly swipe your right middle finger on the fingerprint sensor."
msgstr ""
"Mesedez, arin kolpatu zure eskuineko hatz luzea, behin eta berriz, hatz-"
"marka sentsorean."

#: src/ui/FingerprintDialog.qml:120
#, kde-format
msgid ""
"Please repeatedly swipe your right ring finger on the fingerprint sensor."
msgstr ""
"Mesedez, arin kolpatu zure eskuineko hatz nagia, behin eta berriz,  hatz-"
"marka sentsorean."

#: src/ui/FingerprintDialog.qml:122
#, kde-format
msgid ""
"Please repeatedly swipe your right little finger on the fingerprint sensor."
msgstr ""
"Mesedez, arin kolpatu zure eskuineko hatz txikia, behin eta berriz, hatz-"
"marka sentsorean."

#: src/ui/FingerprintDialog.qml:124
#, kde-format
msgid "Please repeatedly swipe your right thumb on the fingerprint sensor."
msgstr ""
"Mesedez, arin kolpatu zure eskuineko erpurua, behin eta berriz, hatz-marka "
"sentsorean."

#: src/ui/FingerprintDialog.qml:126
#, kde-format
msgid ""
"Please repeatedly swipe your left index finger on the fingerprint sensor."
msgstr ""
"Mesedez, arin kolpatu zure ezkerreko hatz erakuslea, behin eta berriz,  hatz-"
"marka sentsorean."

#: src/ui/FingerprintDialog.qml:128
#, kde-format
msgid ""
"Please repeatedly swipe your left middle finger on the fingerprint sensor."
msgstr ""
"Mesedez, arin kolpatu zure ezkerreko hatz luzea, behin eta berriz, hatz-"
"marka sentsorean."

#: src/ui/FingerprintDialog.qml:130
#, kde-format
msgid ""
"Please repeatedly swipe your left ring finger on the fingerprint sensor."
msgstr ""
"Mesedez, arin kolpatu zure ezkerreko hatz nagia, behin eta berriz,  hatz-"
"marka sentsorean."

#: src/ui/FingerprintDialog.qml:132
#, kde-format
msgid ""
"Please repeatedly swipe your left little finger on the fingerprint sensor."
msgstr ""
"Mesedez, arin kolpatu zure ezkerreko hatz txikia, behin eta berriz, hatz-"
"marka sentsorean."

#: src/ui/FingerprintDialog.qml:134
#, kde-format
msgid "Please repeatedly swipe your left thumb on the fingerprint sensor."
msgstr ""
"Mesedez, arin kolpatu zure ezkerreko erpurua, behin eta berriz, hatz-marka "
"sentsorean."

#: src/ui/FingerprintDialog.qml:150
#, kde-format
msgid "Finger Enrolled"
msgstr "Hatza erroldatu da"

#: src/ui/FingerprintDialog.qml:185
#, kde-format
msgid "Pick a finger to enroll"
msgstr "Hautatu hatz bat erroldatzeko"

#: src/ui/FingerprintDialog.qml:304
#, kde-format
msgid "Re-enroll finger"
msgstr "Hatza berriz erroldatu"

#: src/ui/FingerprintDialog.qml:311
#, kde-format
msgid "Delete fingerprint"
msgstr "Ezabatu hatz-markak"

#: src/ui/FingerprintDialog.qml:320
#, kde-format
msgid "No fingerprints added"
msgstr "Ez da hatz-markarik gehitu"

#: src/ui/main.qml:20
#, kde-format
msgid "Users"
msgstr "Erabiltzaileak"

#: src/ui/main.qml:31
#, kde-format
msgctxt "@action:button As in, 'add new user'"
msgid "Add New"
msgstr "Gehitu ... berria"

#: src/ui/main.qml:107
#, kde-format
msgctxt "@info:usagetip"
msgid "Press Space to edit the user profile"
msgstr "Sakatu zuriunea erabiltzailearen profila editatzeko"

#: src/ui/PicturesSheet.qml:18
#, kde-format
msgctxt "@title"
msgid "Change Avatar"
msgstr "Aldatu abatarra"

#: src/ui/PicturesSheet.qml:22
#, kde-format
msgctxt "@item:intable"
msgid "It's Nothing"
msgstr "Ez da ezer"

#: src/ui/PicturesSheet.qml:23
#, kde-format
msgctxt "@item:intable"
msgid "Feisty Flamingo"
msgstr "Flamenko bizia-bizia"

#: src/ui/PicturesSheet.qml:24
#, kde-format
msgctxt "@item:intable"
msgid "Dragon's Fruit"
msgstr "Herensugearen fruta"

#: src/ui/PicturesSheet.qml:25
#, kde-format
msgctxt "@item:intable"
msgid "Sweet Potato"
msgstr "Batata"

#: src/ui/PicturesSheet.qml:26
#, kde-format
msgctxt "@item:intable"
msgid "Ambient Amber"
msgstr "Giroko anbarera"

#: src/ui/PicturesSheet.qml:27
#, kde-format
msgctxt "@item:intable"
msgid "Sparkle Sunbeam"
msgstr "Eguzki-izpi distiratsua"

#: src/ui/PicturesSheet.qml:28
#, kde-format
msgctxt "@item:intable"
msgid "Lemon-Lime"
msgstr "Lima-limoia"

#: src/ui/PicturesSheet.qml:29
#, kde-format
msgctxt "@item:intable"
msgid "Verdant Charm"
msgstr "Xarma berdexka"

#: src/ui/PicturesSheet.qml:30
#, kde-format
msgctxt "@item:intable"
msgid "Mellow Meadow"
msgstr "Larre baketsua"

#: src/ui/PicturesSheet.qml:31
#, kde-format
msgctxt "@item:intable"
msgid "Tepid Teal"
msgstr "Berde-urdinxka epela"

#: src/ui/PicturesSheet.qml:32
#, kde-format
msgctxt "@item:intable"
msgid "Plasma Blue"
msgstr "Plasma urdina"

#: src/ui/PicturesSheet.qml:33
#, kde-format
msgctxt "@item:intable"
msgid "Pon Purple"
msgstr "Pon purpura"

#: src/ui/PicturesSheet.qml:34
#, kde-format
msgctxt "@item:intable"
msgid "Bajo Purple"
msgstr "Bajo purpura"

#: src/ui/PicturesSheet.qml:35
#, kde-format
msgctxt "@item:intable"
msgid "Burnt Charcoal"
msgstr "Erretako egur-ikatza"

#: src/ui/PicturesSheet.qml:36
#, kde-format
msgctxt "@item:intable"
msgid "Paper Perfection"
msgstr "Papereko perfekzioa"

#: src/ui/PicturesSheet.qml:37
#, kde-format
msgctxt "@item:intable"
msgid "Cafétera Brown"
msgstr "Kafeontziko marroia"

#: src/ui/PicturesSheet.qml:38
#, kde-format
msgctxt "@item:intable"
msgid "Rich Hardwood"
msgstr "Hostozabalen zur bizia"

#: src/ui/PicturesSheet.qml:60
#, kde-format
msgctxt "@action:button"
msgid "Go Back"
msgstr "Joan atzera"

#: src/ui/PicturesSheet.qml:78
#, kde-format
msgctxt "@action:button"
msgid "Initials"
msgstr "Inizialak"

#: src/ui/PicturesSheet.qml:132
#, kde-format
msgctxt "@action:button"
msgid "Choose File…"
msgstr "Hautatu fitxategia..."

#: src/ui/PicturesSheet.qml:137
#, kde-format
msgctxt "@title"
msgid "Choose a picture"
msgstr "Hautatu irudi bat"

#: src/ui/PicturesSheet.qml:185
#, kde-format
msgctxt "@action:button"
msgid "Placeholder Icon"
msgstr "Leku-markaren ikonoa"

#: src/ui/PicturesSheet.qml:274
#, kde-format
msgctxt "@info:whatsthis"
msgid "User avatar placeholder icon"
msgstr "Erabiltzaile-abatarraren leku-markaren ikonoa"

#: src/ui/UserDetailsPage.qml:111
#, kde-format
msgid "Change avatar"
msgstr "Aldatu abatarra"

#: src/ui/UserDetailsPage.qml:164
#, kde-format
msgid "Email address:"
msgstr "E-posta helbidea:"

#: src/ui/UserDetailsPage.qml:193
#, kde-format
msgid "Delete files"
msgstr "Ezabatu fitxategiak"

#: src/ui/UserDetailsPage.qml:200
#, kde-format
msgid "Keep files"
msgstr "Mantendu fitxategiak"

#: src/ui/UserDetailsPage.qml:207
#, kde-format
msgid "Delete User…"
msgstr "Ezabatu erabiltzailea..."

#: src/ui/UserDetailsPage.qml:219
#, kde-format
msgid "Configure Fingerprint Authentication…"
msgstr "Konfiguratu hatz-marka autentifikazioa..."

#: src/ui/UserDetailsPage.qml:248
#, kde-kuit-format
msgctxt "@info"
msgid ""
"Fingerprints can be used in place of a password when unlocking the screen "
"and providing administrator permissions to applications and command-line "
"programs that request them.<nl/><nl/>Logging into the system with your "
"fingerprint is not yet supported."
msgstr ""
"Hatz-markak erabil daitezke pasahitzen ordez pantaila giltzapetik askatzeko "
"eta behar duten aplikazio eta komando-erroko programei administratzaile "
"baimenak emateko.<nl/><nl/>Zure sisteman saio-hastea ez da onartzen oraindik."

#: src/user.cpp:290
#, kde-format
msgid "Could not get permission to save user %1"
msgstr "Ezin izan da %1 erabiltzailea gordetzeko baimenik lortu"

#: src/user.cpp:295
#, kde-format
msgid "There was an error while saving changes"
msgstr "Errore bat gertatu da aldaketak gordetzean"

#: src/user.cpp:394
#, kde-format
msgid "Failed to resize image: opening temp file failed"
msgstr ""
"Irudiaren neurria aldatzea huts egin du: aldi baterako fitxategia irekitzea "
"huts egin du."

#: src/user.cpp:402
#, kde-format
msgid "Failed to resize image: writing to temp file failed"
msgstr ""
"Irudiaren neurria aldatzea huts egin du: aldi baterako fitxategian gordetzea "
"huts egin du."

#: src/usermodel.cpp:147
#, kde-format
msgid "Your Account"
msgstr "Zure kontua"

#: src/usermodel.cpp:147
#, kde-format
msgid "Other Accounts"
msgstr "Beste kontu batzuk"

#~ msgid "Manage Users"
#~ msgstr "Kudeatu erabiltzaileak"

#~ msgid "Continue"
#~ msgstr "Jarraitu"

#~ msgid "Please repeatedly "
#~ msgstr "Mesedez, behin eta berriz"

#~ msgctxt "Example email address"
#~ msgid "john.doe@kde.org"
#~ msgstr "jone.agirre@kde.org"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Iñigo Salvador Azurmendi"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "xalba@euskalnet.net"

#~ msgid "Manage user accounts"
#~ msgstr "Kudeatu erabiltzaile-kontuak"

#~ msgid "Nicolas Fella"
#~ msgstr "Nicolas Fella"

#~ msgid "Carson Black"
#~ msgstr "Carson Black"

#~ msgid "Devin Lin"
#~ msgstr "Devin Lin"

#~ msgctxt "Example name"
#~ msgid "John Doe"
#~ msgstr "Jone Agirre"
