# Copyright (C) 2024 This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
#
# Sveinn í Felli <sv1@fellsnet.is>, 2022.
# SPDX-FileCopyrightText: 2022, 2024 Guðmundur Erlingsson <gudmundure@gmail.com>
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-03-14 00:39+0000\n"
"PO-Revision-Date: 2024-02-19 18:30+0000\n"
"Last-Translator: Guðmundur Erlingsson <gudmundure@gmail.com>\n"
"Language-Team: Icelandic <kde-i18n-doc@kde.org>\n"
"Language: is\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.08.3\n"

#: ui/DayNightView.qml:116
#, kde-format
msgid ""
"Color temperature begins changing to night time at %1 and is fully changed "
"by %2"
msgstr ""
"Lithitastig byrjar að breytast í næturtíma kl. %1 og er að fullu virkt kl. %2"

#: ui/DayNightView.qml:119
#, kde-format
msgid ""
"Color temperature begins changing to day time at %1 and is fully changed by "
"%2"
msgstr ""
"Lithitastig byrjar að breytast í dagtíma kl. %1 og er að fullu virkt kl. %2"

#: ui/LocationsFixedView.qml:39
#, kde-format
msgctxt ""
"@label:chooser Tap should be translated to mean touching using a touchscreen"
msgid "Tap to choose your location on the map."
msgstr "Pikkaðu til að velja staðsetningu þína á kortinu."

#: ui/LocationsFixedView.qml:40
#, kde-format
msgctxt ""
"@label:chooser Click should be translated to mean clicking using a mouse"
msgid "Click to choose your location on the map."
msgstr "Smelltu til að velja staðsetningu þína á kortinu."

#: ui/LocationsFixedView.qml:79 ui/LocationsFixedView.qml:104
#, kde-format
msgid "Zoom in"
msgstr "Auka aðdrátt"

#: ui/LocationsFixedView.qml:210
#, kde-kuit-format
msgctxt "@info"
msgid ""
"Modified from <link url='https://commons.wikimedia.org/wiki/File:"
"World_location_map_(equirectangular_180).svg'>World location map</link> by "
"TUBS / Wikimedia Commons / <link url='https://creativecommons.org/licenses/"
"by-sa/3.0'>CC BY-SA 3.0</link>"
msgstr ""
"Breytt frá <link url='https://commons.wikimedia.org/wiki/File:"
"World_location_map_(equirectangular_180).svg'>Heimskorti staðsetninga</link> "
"af TUBS / Wikimedia Commons / <link url='https://creativecommons.org/"
"licenses/by-sa/3.0'>CC BY-SA 3.0</link>"

#: ui/LocationsFixedView.qml:223
#, kde-format
msgctxt "@label: textbox"
msgid "Latitude:"
msgstr "Lengdargráða:"

#: ui/LocationsFixedView.qml:250
#, kde-format
msgctxt "@label: textbox"
msgid "Longitude:"
msgstr "Breiddargráða:"

#: ui/main.qml:98
#, kde-format
msgid "The blue light filter makes the colors on the screen warmer."
msgstr "Bláljósasía stillir skjáinn á hlýrri liti."

#: ui/main.qml:154
#, kde-format
msgid "Switching times:"
msgstr "Virkjunartími:"

#: ui/main.qml:157
#, kde-format
msgid "Always off"
msgstr "Alltaf slökkt"

#: ui/main.qml:158
#, kde-format
msgid "Sunset and sunrise at current location"
msgstr "Frá sólarlagi til sólarupprásar á núverandi staðsetningu"

#: ui/main.qml:159
#, kde-format
msgid "Sunset and sunrise at manual location"
msgstr "Frá sólarlagi til sólarupprásar á handvirkri staðsetningu"

#: ui/main.qml:160
#, kde-format
msgid "Custom times"
msgstr "Sérstilltur tími"

#: ui/main.qml:161
#, kde-format
msgid "Always on night light"
msgstr "Alltaf kveikt á næturlitum"

#: ui/main.qml:184
#, kde-format
msgid "Day light temperature:"
msgstr "Lithitastig dagslita:"

#: ui/main.qml:227 ui/main.qml:289
#, kde-format
msgctxt "Color temperature in Kelvin"
msgid "%1K"
msgstr "%1K"

#: ui/main.qml:232 ui/main.qml:294
#, kde-format
msgctxt "Night colour blue-ish; no blue light filter activated"
msgid "Cool (no filter)"
msgstr "Svalt (engin sía)"

#: ui/main.qml:239 ui/main.qml:301
#, kde-format
msgctxt "Night colour red-ish"
msgid "Warm"
msgstr "Hlýtt"

#: ui/main.qml:246
#, kde-format
msgid "Night light temperature:"
msgstr "Lithitastig næturlita:"

#: ui/main.qml:311
#, kde-format
msgctxt "@label The coordinates for the current location"
msgid "Current location:"
msgstr "Núverandi staðsetning:"

#: ui/main.qml:317
#, kde-format
msgid "Latitude: %1°   Longitude: %2°"
msgstr "Breiddargráða: %1°   Lengdargráða: %2°"

#: ui/main.qml:339
#, kde-kuit-format
msgctxt "@info"
msgid ""
"The device's location will be periodically updated using GPS (if available), "
"or by sending network information to <link url='https://location.services."
"mozilla.com'>Mozilla Location Service</link>."
msgstr ""
"Staðsetning þessa tækis verður uppfærð öðru hvoru með GPS (ef tiltækt), eða "
"með því að senda upplýsingar um netkerfi til <link url='https://location."
"services.mozilla.com'>Mozilla-staðsetningarþjónustunnar</link>."

#: ui/main.qml:357
#, kde-format
msgid "Begin night light at:"
msgstr "Byrja næturliti kl.:"

#: ui/main.qml:370 ui/main.qml:393
#, kde-format
msgid "Input format: HH:MM"
msgstr "Inntakssnið: HH:MM"

#: ui/main.qml:380
#, kde-format
msgid "Begin day light at:"
msgstr "Byrja dagsliti kl.:"

#: ui/main.qml:402
#, kde-format
msgid "Transition duration:"
msgstr "Tímalengd yfirfærslu:"

#: ui/main.qml:411
#, kde-format
msgid "%1 minute"
msgid_plural "%1 minutes"
msgstr[0] "%1 mínúta"
msgstr[1] "%1 mínútur"

#: ui/main.qml:424
#, kde-format
msgid "Input minutes - min. 1, max. 600"
msgstr "Sláðu inn mínútur - lágm. 1, hám. 600"

#: ui/main.qml:443
#, kde-format
msgid "Error: Transition time overlaps."
msgstr "Villa: Yfirfærslutímar skarast."

#: ui/main.qml:466
#, kde-format
msgctxt "@info:placeholder"
msgid "Locating…"
msgstr "Staðset…"

#~ msgctxt "Night colour blue-ish"
#~ msgid "Cool"
#~ msgstr "Svalt"

#~| msgid "This is what Night Color will look like when active."
#~ msgid "This is what day color temperature will look like when active."
#~ msgstr "Svona munu dagslitir líta út þegar þeir eru virkir."

#~| msgid "This is what Night Color will look like when active."
#~ msgid "This is what night color temperature will look like when active."
#~ msgstr "Svona munu næturlitir líta út þegar þeir eru virkir."

#, fuzzy
#~| msgid "Activate Night Color"
#~ msgid "Activate blue light filter"
#~ msgstr "Virkja næturliti"

#~ msgid "Turn on at:"
#~ msgstr "Kveikja kl.:"

#~ msgid "Turn off at:"
#~ msgstr "Slökkva kl.:"

#~ msgid "Night Color begins changing back at %1 and ends at %2"
#~ msgstr ""
#~ "Næturlitir byrja að breytast til baka kl. %1 og eru að fullu óvirkir kl. "
#~ "%2"

#~ msgid "Error: Morning is before evening."
#~ msgstr "Villa; Morgunn er á undan kvöldi."
