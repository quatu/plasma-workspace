# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# SPDX-FileCopyrightText: 2022, 2023 Emir SARI <emir_sari@icloud.com>
# Serdar Soytetir, 2008.
# Serdar Soytetir <tulliana@gmail.com>, 2008, 2010, 2013.
# Volkan Gezer <volkangezer@gmail.com>, 2013, 2021, 2022.
# Kaan Ozdincer <kaanozdincer@gmail.com>, 2014.
# Emir SARI <emir_sari@icloud.com>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: kcm_autostart\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-02-18 00:39+0000\n"
"PO-Revision-Date: 2023-11-11 23:26+0300\n"
"Last-Translator: Emir SARI <emir_sari@icloud.com>\n"
"Language-Team: Turkish <kde-l10n-tr@kde.org>\n"
"Language: tr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 23.11.70\n"

#: autostartmodel.cpp:376
#, kde-format
msgid "\"%1\" is not an absolute url."
msgstr "\"%1\" mutlak bir URL değil."

#: autostartmodel.cpp:379
#, kde-format
msgid "\"%1\" does not exist."
msgstr "\"%1\" yok."

#: autostartmodel.cpp:382
#, kde-format
msgid "\"%1\" is not a file."
msgstr "\"%1\" bir dosya değil."

#: autostartmodel.cpp:385
#, kde-format
msgid "\"%1\" is not readable."
msgstr "\"%1\" okunabilir değil."

#: ui/entry.qml:52
#, kde-format
msgctxt ""
"@label The name of a Systemd unit for an app or script that will autostart"
msgid "Name:"
msgstr "Ad:"

#: ui/entry.qml:58
#, kde-format
msgctxt ""
"@label The current status (e.g. active or inactive) of a Systemd unit for an "
"app or script that will autostart"
msgid "Status:"
msgstr "Durum:"

#: ui/entry.qml:64
#, kde-format
msgctxt ""
"@label A date and time follows this text, making a sentence like 'Last "
"activated on: August 7th 11 PM 2023'"
msgid "Last activated on:"
msgstr "Son etkinleştirme:"

#: ui/entry.qml:74
#, kde-format
msgctxt "@label Stop the Systemd unit for a running process"
msgid "Stop"
msgstr "Durdur"

#: ui/entry.qml:74
#, kde-format
msgctxt "@label Start the Systemd unit for a currently inactive process"
msgid "Start"
msgstr "Başlat"

#: ui/entry.qml:109
#, kde-format
msgid "Unable to load logs. Try refreshing."
msgstr "Günlükler yüklenemiyor. Yenilemeyi deneyin."

#: ui/entry.qml:113
#, kde-format
msgctxt "@action:button Refresh entry logs when it failed to load"
msgid "Refresh"
msgstr "Yenile"

#: ui/main.qml:33
#, kde-format
msgid "Make Executable"
msgstr "Yürütülebilir Yap"

#: ui/main.qml:53
#, kde-format
msgid "The file '%1' must be executable to run at logout."
msgstr ""
"\"%1\" dosyası, oturum kapanışında çalıştırılabilmesi için yürütülebilir "
"olmalıdır."

#: ui/main.qml:55
#, kde-format
msgid "The file '%1' must be executable to run at login."
msgstr ""
"\"%1\" dosyası, oturum açılışında çalıştırılabilmesi için yürütülebilir "
"olmalıdır."

#: ui/main.qml:66
#, kde-format
msgctxt "@action:button"
msgid "Add…"
msgstr "Ekle…"

#: ui/main.qml:69
#, kde-format
msgctxt "@action:button"
msgid "Add Application…"
msgstr "Uygulama Ekle…"

#: ui/main.qml:74
#, kde-format
msgctxt "@action:button"
msgid "Add Login Script…"
msgstr "Oturum Açma Betiği Ekle…"

#: ui/main.qml:79
#, kde-format
msgctxt "@action:button"
msgid "Add Logout Script…"
msgstr "Oturum Kapatma Betiği Ekle…"

#: ui/main.qml:114
#, kde-format
msgid ""
"%1 has not been autostarted yet. Details will be available after the system "
"is restarted."
msgstr ""
"%1 henüz kendiliğinden başlatılmadı. Ayrıntılar sistem yeniden "
"başlatıldıktan sonra kullanılabilir olacak."

#: ui/main.qml:137
#, kde-format
msgctxt ""
"@label Entry hasn't been autostarted because system hasn't been restarted"
msgid "Not autostarted yet"
msgstr "Henüz kendiliğinden başlatılmadı"

#: ui/main.qml:146
#, kde-format
msgctxt "@action:button"
msgid "See properties"
msgstr "Özellikleri Gör"

#: ui/main.qml:157
#, kde-format
msgctxt "@action:button"
msgid "Remove entry"
msgstr "Girdiyi Kaldır"

#: ui/main.qml:173
#, kde-format
msgid "Applications"
msgstr "Uygulamalar"

#: ui/main.qml:176
#, kde-format
msgid "Login Scripts"
msgstr "Oturum Açma Betikleri"

#: ui/main.qml:179
#, kde-format
msgid "Pre-startup Scripts"
msgstr "Başlangıç Öncesi Betikleri"

#: ui/main.qml:182
#, kde-format
msgid "Logout Scripts"
msgstr "Oturum Kapatma Betikleri"

#: ui/main.qml:191
#, kde-format
msgid "No user-specified autostart items"
msgstr "Kullanıcı tanımlı kendiliğinden başlat ögesi yok"

#: ui/main.qml:192
#, kde-kuit-format
msgctxt "@info 'some' refers to autostart items"
msgid "Click the <interface>Add…</interface> button to add some"
msgstr ""
"Birkaç tane eklemek için <interface>Ekle…</interface> düğmesine tıklayın"

#: ui/main.qml:207
#, kde-format
msgid "Choose Login Script"
msgstr "Oturum Açma Betiği Seç"

#: ui/main.qml:227
#, kde-format
msgid "Choose Logout Script"
msgstr "Oturum Kapatma Betiği Seç"

#: unit.cpp:26
#, kde-format
msgctxt "@label Entry is running right now"
msgid "Running"
msgstr "Çalışıyor"

#: unit.cpp:27
#, kde-format
msgctxt "@label Entry is not running right now (exited without error)"
msgid "Not running"
msgstr "Çalışmıyor"

#: unit.cpp:28
#, kde-format
msgctxt "@label Entry is being started"
msgid "Starting"
msgstr "Başlatılıyor"

#: unit.cpp:29
#, kde-format
msgctxt "@label Entry is being stopped"
msgid "Stopping"
msgstr "Durduruluyor"

#: unit.cpp:30
#, kde-format
msgctxt "@label Entry has failed (exited with an error)"
msgid "Failed"
msgstr "Başarısız oldu"

#: unit.cpp:83
#, kde-format
msgid "Error occurred when receiving reply of GetAll call %1"
msgstr "%1 GetAll çağrısının yanıtı alınırken bir hata oluştu"

#: unit.cpp:155
#, kde-format
msgid "Failed to open journal"
msgstr "Günce açılamadı"
