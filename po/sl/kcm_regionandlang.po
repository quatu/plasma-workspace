# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
#
# Matjaž Jeran <matjaz.jeran@amis.net>, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-03-14 00:39+0000\n"
"PO-Revision-Date: 2023-02-12 08:44+0100\n"
"Last-Translator: Matjaž Jeran <matjaz.jeran@amis.net>\n"
"Language-Team: Slovenian <lugos-slo@lugos.si>\n"
"Language: sl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n"
"%100==4 ? 3 : 0);\n"
"X-Generator: Lokalize 22.12.2\n"

#: exampleutility.cpp:29
#, kde-format
msgctxt "Measurement combobox"
msgid "Imperial UK"
msgstr "Imperial UK"

#: exampleutility.cpp:31
#, kde-format
msgctxt "Measurement combobox"
msgid "Imperial US"
msgstr "Imperial US"

#: exampleutility.cpp:33
#, kde-format
msgctxt "Measurement combobox"
msgid "Metric"
msgstr "Metrično"

#: exampleutility.cpp:47
#, kde-format
msgctxt "PaperSize combobox"
msgid "Letter"
msgstr "Pismo"

#: exampleutility.cpp:49
#, kde-format
msgctxt "PaperSize combobox"
msgid "A4"
msgstr "A4"

#: exampleutility.cpp:64
#, kde-format
msgctxt "Care of person or organization"
msgid "c/o"
msgstr "za"

#: exampleutility.cpp:65
#, kde-format
msgctxt "Firm name"
msgid "Acme Corporation"
msgstr "Ime tvrdke"

#: exampleutility.cpp:66
#, kde-format
msgctxt "Department name"
msgid "Development Department"
msgstr "Razvojni oddelek"

#: exampleutility.cpp:67
#, kde-format
msgctxt "Building name"
msgid "Dev-Building"
msgstr "Ime stavbe"

#: exampleutility.cpp:68
#, kde-format
msgctxt "Street or block name"
msgid "Main Street"
msgstr "Glavna ulica"

#: exampleutility.cpp:69
#, kde-format
msgctxt "House number"
msgid "House 1"
msgstr "Hiša 1"

#: exampleutility.cpp:71
#, kde-format
msgctxt "Whitespace field for locale address style example"
msgid " "
msgstr " "

#: exampleutility.cpp:72
#, kde-format
msgctxt "Room number"
msgid "Room 2"
msgstr "Soba 2"

#: exampleutility.cpp:73
#, kde-format
msgctxt "Floor number"
msgid "Floor 3"
msgstr "Tretje nadstropje"

#: exampleutility.cpp:75
#, kde-format
msgctxt "Local township within town or city"
msgid "Downtown"
msgstr "Zgornja Šiška"

#: exampleutility.cpp:76
#, kde-format
msgctxt "Zip number, postal code"
msgid "123456"
msgstr "123456"

#: exampleutility.cpp:77
#, kde-format
msgctxt "Town or city"
msgid "City"
msgstr "Mesto"

#: exampleutility.cpp:78
#, kde-format
msgctxt "State, province or prefecture"
msgid "State"
msgstr "Pokrajina"

#: exampleutility.cpp:91
#, kde-format
msgctxt "Family names"
msgid "FamilyName"
msgstr "Družinsko ime"

#: exampleutility.cpp:92
#, kde-format
msgctxt "Family names in uppercase"
msgid "FAMILYNAME"
msgstr "DRUŽINA Z VELIKO"

#: exampleutility.cpp:93
#, kde-format
msgctxt "First given name"
msgid "FirstName"
msgstr "Osebno ime"

#: exampleutility.cpp:94
#, kde-format
msgctxt "First given initial"
msgid "F"
msgstr "Inicialka"

#: exampleutility.cpp:95
#, kde-format
msgctxt "First given name with latin letters"
msgid "FirstName"
msgstr "Osebno ime v latinici"

#: exampleutility.cpp:96
#, kde-format
msgctxt "Other shorter name"
msgid "OtherName"
msgstr "Drugo ime"

#: exampleutility.cpp:97
#, kde-format
msgctxt "Additional given names"
msgid "AdditionalName"
msgstr "Dodatna imena"

#: exampleutility.cpp:98
#, kde-format
msgctxt "Initials for additional given names"
msgid "A"
msgstr "A"

#: exampleutility.cpp:99
#, kde-format
msgctxt "Profession"
msgid "Profession"
msgstr "Poklic"

#: exampleutility.cpp:100
#, kde-format
msgctxt "Salutation"
msgid "Doctor"
msgstr "Doktor"

#: exampleutility.cpp:101
#, kde-format
msgctxt "Abbreviated salutation"
msgid "Dr."
msgstr "Dr."

#: exampleutility.cpp:102
#, kde-format
msgctxt "Salutation using the FDCC-sets conventions"
msgid "Dr."
msgstr "Dr."

#: exampleutility.cpp:103
#, kde-format
msgctxt "Space or dot for locale name style example"
msgid " "
msgstr " "

#: exampleutility.cpp:119
#, kde-format
msgctxt "Whitespace for telephone style example"
msgid " "
msgstr " "

#: exampleutility.cpp:133
#, kde-format
msgctxt ""
"This is returned when an example test could not be made from locale "
"information"
msgid "Could not find an example for this locale"
msgstr "Ni bilo mogoče najti primera za to"

#: kcmregionandlang.cpp:116
#, kde-kuit-format
msgctxt "@info this will be shown as an error message"
msgid ""
"Could not find the system's available locales using the <command>localectl</"
"command> tool. Please file a bug report about this at <link>https://bugs.kde."
"org</link>"
msgstr ""
"Ni bilo mogoče najti razpoložljivih lokalizacij sistema z ukazom orodja "
"<command>localectl</command>. Prosimo, da o tem vložite poročilo na "
"<link>https://bugs.kde.org</link>"

#: languagelistmodel.cpp:87
#, kde-format
msgctxt "%1 is language name, %2 is language code name"
msgid "%1 (%2)"
msgstr "%1 (%2)"

#: languagelistmodel.cpp:93
#, kde-format
msgctxt ""
"%1 is português in system locale name, Brazil is to distinguish European "
"português and Brazilian português"
msgid "%1 (Brazil)"
msgstr "%1 (Brazilsko)"

#: localegeneratorubuntu.cpp:42
#, kde-format
msgctxt "@info:warning"
msgid "Can't locate executable `check-language-support`"
msgstr "Ni mogoče najti izvedljivega programa `check-language-support`"

#: localegeneratorubuntu.cpp:52
#, kde-format
msgctxt "the arg is the output of failed check-language-support call"
msgid "check-language-support failed, output: %1"
msgstr "check-language-support ni uspel, izhod: %1"

#: localegeneratorubuntu.cpp:83
#, kde-format
msgctxt "%1 is a list of package names"
msgid "Not all missing packages managed to resolve! %1"
msgstr "Nekateri manjkajoči paketi se ne uspejo razrešiti! %1"

#: localegeneratorubuntu.cpp:89
#, kde-format
msgctxt "%1 is a list of package names"
msgid "Failed to install package %1"
msgstr "Ni bilo mogoče namestiti paketa %1"

#: localegenhelper/localegenhelper.cpp:43
#, kde-format
msgid "Another process is already running, please retry later"
msgstr "Drug proces že teče, poskusite kasneje"

#: localegenhelper/localegenhelper.cpp:63
#, kde-format
msgid "Unauthorized to edit locale configuration file"
msgstr "Nimate pravic za urejanje konfiguracijske datoteke locale"

#: localegenhelper/localegenhelper.cpp:113
#, kde-format
msgid "Can't open file `/etc/locale.gen`"
msgstr "NI mogoče odpreti datoteke `/etc/locale.gen`"

#: localegenhelper/localegenhelper.cpp:146
#, kde-format
msgid "Can't locate executable `locale-gen`"
msgstr "Ni mogoče najti izvedljivega programa 'locale-gen`"

#: localegenhelper/localegenhelper.cpp:159
#, kde-format
msgid "Unknown"
msgstr "Neznano"

#: localelistmodel.cpp:22
#, kde-format
msgid "Default for System"
msgstr "Privzeto za sistem"

#: localelistmodel.cpp:22 localelistmodel.cpp:71 ui/main.qml:287
#, kde-format
msgid "Default"
msgstr "Privzeto"

#: localelistmodel.cpp:67
#, kde-format
msgctxt ""
"the first is language name, the second is the country name, like 'English "
"(America)'"
msgid "%1 (%2)"
msgstr "%1 (%2)"

#: localelistmodel.cpp:150
#, kde-format
msgctxt ""
"@info:title, meaning the current locale is system default(which is `C`)"
msgid "System Default C"
msgstr "Sistemsko privzeto C"

#: localelistmodel.cpp:153
#, kde-format
msgctxt ""
"@info:title the current locale is the default for %1, %1 is the country name"
msgid "Default for %1"
msgstr "Privzeto za %1"

#: optionsmodel.cpp:23
#, kde-format
msgctxt "@info:title"
msgid "Language"
msgstr "Jezik"

#: optionsmodel.cpp:24
#, kde-format
msgctxt "@info:title"
msgid "Numbers"
msgstr "Števila"

#: optionsmodel.cpp:25
#, kde-format
msgctxt "@info:title"
msgid "Time"
msgstr "Čas"

#: optionsmodel.cpp:26
#, kde-format
msgctxt "@info:title"
msgid "Currency"
msgstr "Valuta"

#: optionsmodel.cpp:27
#, kde-format
msgctxt "@info:title"
msgid "Measurements"
msgstr "Meritve"

#: optionsmodel.cpp:28
#, kde-format
msgctxt "@info:title"
msgid "Paper Size"
msgstr "Velikost papirja"

#: optionsmodel.cpp:30
#, kde-format
msgctxt "@info:title"
msgid "Address"
msgstr "Naslov"

#: optionsmodel.cpp:31
#, kde-format
msgctxt "@info:title"
msgid "Name Style"
msgstr "Slog imena"

#: optionsmodel.cpp:32
#, kde-format
msgctxt "@info:title"
msgid "Phone Numbers"
msgstr "Telefonske številke"

#: optionsmodel.cpp:117 optionsmodel.cpp:310
#, kde-format
msgctxt "@info:title, the current setting is system default"
msgid "System Default"
msgstr "Sistemsko privzeto"

#: optionsmodel.cpp:312
#, kde-format
msgctxt ""
"as subtitle, remind user that the format used now is inherited from locale %1"
msgid " (Standard format for %1)"
msgstr " (Standardni format za %1)"

#: regionandlangsettingsbase.kcfg:18 regionandlangsettingsbase.kcfg:21
#: regionandlangsettingsbase.kcfg:24 regionandlangsettingsbase.kcfg:27
#: regionandlangsettingsbase.kcfg:30 regionandlangsettingsbase.kcfg:33
#: regionandlangsettingsbase.kcfg:36 regionandlangsettingsbase.kcfg:39
#: regionandlangsettingsbase.kcfg:42
#, kde-format
msgid "Inherit Language"
msgstr "Podeduj jezik"

#: ui/AdvancedLanguageSelectPage.qml:20
#, kde-format
msgid "Language"
msgstr "Jezik"

#: ui/AdvancedLanguageSelectPage.qml:31
#, kde-format
msgid ""
"Putting any other languages below English will cause undesired behavior in "
"some applications. If you would like to use your system in English, remove "
"all other languages."
msgstr ""
"Postavitev kateregakoli drugega jezika pod angleščino bo povzročilo neželeno "
"vedenje nekaterih aplikacij. Če želite, da vaš sistem uporablja angleščino, "
"odstranite vse druge jezike."

#: ui/AdvancedLanguageSelectPage.qml:39
#, kde-format
msgctxt "Error message, %1 is the language name"
msgid "The language \"%1\" is unsupported"
msgstr "Jezik \"%1\" ni podprt"

#: ui/AdvancedLanguageSelectPage.qml:49
#, kde-format
msgid "Add languages in the order you want to see them in your applications."
msgstr ""
"Dodajte jezike v vrstnem redu, kakor jih želite videti v vaših aplikacijah."

#: ui/AdvancedLanguageSelectPage.qml:87
#, kde-format
msgctxt "@info:tooltip"
msgid "Change Language"
msgstr "Spremeni jezik"

#: ui/AdvancedLanguageSelectPage.qml:91
#, kde-format
msgctxt "@title:window"
msgid "Change Language"
msgstr "Spremeni jezik"

#: ui/AdvancedLanguageSelectPage.qml:102
#, kde-format
msgctxt "@info:tooltip"
msgid "Move to top"
msgstr "Postavi na vrh"

#: ui/AdvancedLanguageSelectPage.qml:108
#, kde-format
msgctxt "@info:tooltip"
msgid "Remove"
msgstr "Odstrani"

#: ui/AdvancedLanguageSelectPage.qml:121
#, kde-format
msgctxt "@info:placeholder"
msgid "No Language Configured"
msgstr "Ni konfiguriranega jezika"

#: ui/AdvancedLanguageSelectPage.qml:152 ui/AdvancedLanguageSelectPage.qml:163
#, kde-format
msgctxt "@title:window"
msgid "Add Languages"
msgstr "Dodaj jezike"

#: ui/AdvancedLanguageSelectPage.qml:180
#, kde-format
msgctxt "@action:button"
msgid "Add More…"
msgstr "Dodaj več…"

#: ui/main.qml:32
#, kde-format
msgctxt "@info"
msgid "Changes will take effect the next time you log in."
msgstr "Spremembe bodo uveljavljene po naslednji prijavi."

#: ui/main.qml:38
#, kde-format
msgid "Restart now"
msgstr "Ponovno zaženi"

#: ui/main.qml:52
#, kde-format
msgctxt "@info"
msgid ""
"Generating locale and language support files; don't turn off the computer "
"yet."
msgstr ""
"Generiranje datoteke locale in podpornih datotek; ne ugašajte še računalnika."

#: ui/main.qml:61
#, kde-format
msgctxt "@info"
msgid ""
"Locale and language support files have been generated, but language-specific "
"fonts could not be automatically installed. If your language requires "
"specialized fonts to be displayed properly, you will need to discover what "
"they are and install them yourself."
msgstr ""
"Generirane so bile locale in druge datoteke jezikovno podporo, vendar je za "
"jezik specifične pisave ni bilo mogoče samodejno namestiti. Če vaš jezik "
"zahtevaspecializirane pisave, da bo jezik prikazan pravilno, jih boste "
"morali sami odkriti in namestiti."

#: ui/main.qml:75
#, kde-format
msgctxt "@info"
msgid ""
"Necessary locale and language support files have been installed. It is now "
"safe to turn off the computer."
msgstr ""
"Nujne datoteke locale in datoteke za podporo jezika so bile nameščene. Zdaj "
"je varno, da ugasnete računalnik."

#: ui/main.qml:169
#, kde-format
msgctxt "@action:button for change the locale used"
msgid "Modify…"
msgstr "Spremeni…"

#: ui/main.qml:209
#, kde-format
msgid "Numbers"
msgstr "Števila"

#: ui/main.qml:211
#, kde-format
msgid "Time"
msgstr "Čas"

#: ui/main.qml:213
#, kde-format
msgid "Currency"
msgstr "Valuta"

#: ui/main.qml:215
#, kde-format
msgid "Measurements"
msgstr "Meritve"

#: ui/main.qml:217
#, kde-format
msgid "Paper Size"
msgstr "Velikost papirja"

#: ui/main.qml:219
#, kde-format
msgctxt "Postal Address"
msgid "Address"
msgstr "Naslov"

#: ui/main.qml:221
#, kde-format
msgctxt "Name Style"
msgid "Name"
msgstr "Ime"

#: ui/main.qml:223
#, kde-format
msgctxt "Phone Numbers"
msgid "Phone number"
msgstr "Telefonska številka"

#~ msgid ""
#~ "You can configure the formats used for time, dates, money and other "
#~ "numbers here."
#~ msgstr ""
#~ "Tu lahko konfigurirate formate za čas, datume, denar in druge številske "
#~ "vrednosti."

#~ msgctxt "@info:warning"
#~ msgid ""
#~ "Locale has been configured, but this KCM currently doesn't support auto "
#~ "locale generation on non-glibc systems, please refer to your "
#~ "distribution's manual to install fonts and generate locales"
#~ msgstr ""
#~ "Locale je konfiguriran, vendar ta KCM trenutno ne podpira samodejno "
#~ "generacija locale v sistemih, ki niso glibc, poglejte v priročnik za "
#~ "svojo distribucijoza namestitev pisav in ustvarjanje locale"

#~ msgctxt "Button for change the locale used"
#~ msgid "Change it…"
#~ msgstr "Spremeni ga…"
